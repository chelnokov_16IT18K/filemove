package ru.chelnokov.main;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс для перемещения файлов по заданным путям
 * Также класс выводит количество и имена перемещённых файлов в консоль.
 *
 * @author Chelnokov E.I. 16IT18k
 */
public class FileMove {

    private static final String INTRO = "Здравствуйте! Чтобы переместить файлы, вам нужно ввести:";
    private static final String SOURCE = "1. Путь к папке, из которой вы собираетесь взять файлы, например\n D:\\\\JavaProjects\\\\TransferFile\\\\src\\\\ru\\\\chelnokov\\\\from:";
    private static final String TARGET = "2. Путь к папке, в которую вы положите файлы, например \n D:\\\\JavaProjects\\\\TransferFile\\\\src\\\\ru\\\\chelnokov\\\\to:";

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println(INTRO);
        System.out.println(SOURCE);
        Path pathSource = Paths.get(reader.nextLine());
        System.out.println(TARGET);
        Path pathDestination = Paths.get(reader.nextLine());
        try {

            Files.move(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
            ArrayList<Path> files = new ArrayList<>();
            listFiles(pathDestination, files);//имена перемещённых файлов
            System.out.println("Перемещение прошло успешно. Перемещённые файлы: ");
            System.out.println(files.toString());
            System.out.printf("Количество перемещённых файлов - %d:\n", files.size());

        } catch (IOException e) {
            System.out.println("Произошла ошибка. Пожалуйста, проверьте правильность заданных путей или наличие файлов для перемещения.");
        }
    }

    /**
     * заполняет исходный ArrayList именами перебранных файлов
     * @param pathDestination путь к папке, куда были перемещены файлы
     * @param files пустой ArrayList
     * @throws IOException ошибки при задании путей
     */
    private static void listFiles(Path pathDestination, ArrayList<Path> files) throws IOException {
        DirectoryStream<Path> stream = Files.newDirectoryStream(pathDestination);
        for (Path entry : stream) {
            if (Files.isDirectory(entry)) {
                listFiles(entry, files);
            }else {
                files.add(entry.getFileName());
            }
        }
    }
}
